import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateTodoDto } from './todo.dto';
import { Todo } from './todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private todoRepo: Repository<Todo>,
  ) {}

  /** Service: Create a todo
   * @param CreateTodoDto the new todo data
   * @param todoist the todoist id
   * @returns Promise<{data: Todo}>
   */
  async createTodo({ title, description, priority }, todoist: any) {
    const todo = new Todo();
    todo.todoist = todoist;
    todo.title = title;
    todo.description = description;
    todo.priority = priority;
    await this.todoRepo.save(todo);
    return { data: todo };
  }

  /** Service: Get a todoist's todos
   * @param todoist the todoist id
   * @returns Promise<{data: Todo[]}>
   */
  async getTodos(todoist: string) {
    return { data: await this.todoRepo.find({ where: { todoist } }) };
  }

  /** Service: Get a single todo
   * @param id the todo's id
   * @param todoist the todoist id
   * @returns Promise<{data: Todo}>
   */
  async getATodo(id: string, todoist: string) {
    return {
      data: await this.findTodo({ where: { id: id, todoist } }),
    };
  }

  /** Service: Update a single todo
   * @param id the todo's id
   * @param UpdateTodoDto the updates
   * @param todoist the todoist id
   * @returns Promise<{data: Todo}>
   */
  async updateTodo(
    id: string,
    { title, description, priority, completed }: UpdateTodoDto,
    todoist: string,
  ) {
    const todo = await this.findTodo({ where: { id: id, todoist } });
    if (title) todo.title = title;
    if (description) todo.description = description;
    if (priority) todo.priority = priority;
    if (completed) todo.completed = completed;
    await this.todoRepo.save(todo);
    return { data: todo };
  }

  /** Service: Delete a single todo
   * @param id todo id
   * @param todoist the todoist id
   * @returns Promise<{message: string}>
   */
  async deleteTodo(id: string, todoist: string) {
    const todo = await this.findTodo({ where: { id: id, todoist } });
    await this.todoRepo.delete(todo);
    return { message: 'Todo deleted' };
  }

  /** Service: Update all todos' completed statuses
   * @param todoist todoist id
   * @returns Promise<{message: string}>
   */
  async updateAllCompletedStatuses(todoist: string) {
    (await this.todoRepo.find({ where: { todoist } })).forEach(async (el) => {
      el.completed = true;
      await this.todoRepo.save(el);
    });
    return { message: 'All todos completed' };
  }

  /** Private Service: Find a todo from db
   * @param options find options
   * @returns Promise<Todo>
   */
  private async findTodo(options: any) {
    const todo = await this.todoRepo.findOne(options);
    if (!todo) throw new NotFoundException('Todo not found');
    else return todo;
  }
}
