import { Todoist } from '../todoist/todoist.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('todos')
export class Todo {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  title: string;

  @Column({ nullable: false })
  description: string;

  @Column({ type: 'enum', enum: ['HIGH', 'MEDIUM', 'LOW'], nullable: false })
  priority: 'HIGH' | 'MEDIUM' | 'LOW';

  @Column({ nullable: false, default: false })
  completed: boolean;

  @CreateDateColumn({
    type: 'timestamptz',
    default: 'NOW()',
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamptz',
    onUpdate: 'NOW()',
  })
  modifiedAt: Date;

  @ManyToOne((type) => Todoist, (user) => user.todos)
  todoist: Todoist;
}
