import { Todoist } from '../todoist/todoist.entity';
import { User } from './../shared/decorators/user.decorator';
import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Patch,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CreateTodoDto, UpdateTodoDto } from './todo.dto';
import { TodoService } from './todo.service';

@ApiTags('Todos')
@ApiBearerAuth()
@Controller('todos')
@UseGuards(AuthGuard())
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  /** Route: Create a todo
   * @param body the request body
   * @param req the request object
   * @returns Todo
   */
  @Post()
  @ApiResponse({ status: 201, description: 'Todo created' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  createTodo(@Body() body: CreateTodoDto, @User() user: Todoist) {
    return this.todoService.createTodo(body, user?.id);
  }

  /** Route: Update all todos' completed statuses
   * @param req the request object
   * @returns message: string
   */
  @Patch('/complete')
  @ApiResponse({ status: 201, description: 'Update successful' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  completeAll(@User() user: Todoist) {
    return this.todoService.updateAllCompletedStatuses(user?.id);
  }

  /** Route: Get a todoist's todos
   * @param req the request object
   * @returns Todo[]
   */
  @Get()
  @ApiResponse({ status: 200, description: 'Todos fetched' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  getTodos(@User() user: Todoist) {
    return this.todoService.getTodos(user?.id);
  }

  /** Route: Get a single todo
   * @param req the request object
   * @param id the todo's id
   * @returns Todo
   */
  @Get('/:id')
  @ApiResponse({ status: 200, description: 'Todo fetched' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  getATodo(@Param('id') id: string, @User() user: Todoist) {
    return this.todoService.getATodo(id, user?.id);
  }

  /** Route: Update a single todo
   * @param req the request object
   * @param id the todo's id
   * @returns Updated Todo
   */
  @Patch('/:id')
  @ApiResponse({ status: 201, description: 'Todo updated' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  updateTodo(
    @Param('id') id: string,
    @Body() body: UpdateTodoDto,
    @User() user: Todoist,
  ) {
    return this.todoService.updateTodo(id, body, user?.id);
  }

  /** Route: Delete a single todo
   * @param req the request object
   * @param id the todo's id
   * @returns message: string
   */
  @Delete('/:id')
  @ApiResponse({ status: 200, description: 'Todo deleted' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  deleteTodo(
    @Param('id') id: string,
    @User() user: Todoist,
  ): Promise<{ message: string }> {
    return this.todoService.deleteTodo(id, user?.id);
  }
}
