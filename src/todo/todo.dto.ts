import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsIn,
  IsOptional,
  IsString,
  Length,
  MinLength,
} from 'class-validator';

export class CreateTodoDto {
  @IsString()
  @Length(5, 100, {
    message: 'Title must be between 5 and 100 characters',
  })
  @ApiProperty({
    description: 'The title of a todo',
    default: 'Jogging',
  })
  title: string;

  @IsString()
  @MinLength(5, {
    message: 'Description must be atleast 5 characters',
  })
  @ApiProperty({
    description: 'The description of a todo',
    default: 'Go out jogging for 5 miles',
  })
  description: string;

  @IsString()
  @IsIn(['HIGH', 'MEDIUM', 'LOW'], {
    message: 'Priority must be one of HIGH, MEDIUM or LOW',
  })
  @ApiProperty({
    enum: ['HIGH', 'MEDIUM', 'LOW'],
    description: 'The priority of a todo',
    default: 'MEDIUM',
  })
  priority: string;
}

export class UpdateTodoDto {
  @IsString()
  @Length(5, 100, {
    message: 'Title must be between 5 and 100 characters',
  })
  @IsOptional()
  @ApiPropertyOptional({
    description: 'The title of a todo',
    default: 'Jogging again',
  })
  title?: string;

  @IsString()
  @MinLength(5, {
    message: 'Description must be atleast 5 characters',
  })
  @IsOptional()
  @ApiPropertyOptional({
    description: 'The description of a todo',
    default: 'Go out jogging for 5 miles again',
  })
  description?: string;

  @IsString()
  @IsIn(['HIGH', 'MEDIUM', 'LOW'], {
    message: 'Priority must be one of HIGH, MEDIUM or LOW',
  })
  @IsOptional()
  @ApiPropertyOptional({
    enum: ['HIGH', 'MEDIUM', 'LOW'],
    description: 'The priority of a todo',
    default: 'HIGH',
  })
  priority?: 'HIGH' | 'MEDIUM' | 'LOW';

  @IsBoolean()
  @IsOptional()
  @ApiPropertyOptional({
    description: 'The completed status of a todo (true or false)',
    default: true,
  })
  completed?: boolean;
}
