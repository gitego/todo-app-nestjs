export interface JwtPayload {
  id: string;
  firstName: string;
  lastName: string;
  username: string;
}
