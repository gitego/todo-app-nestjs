import { JwtPayload } from './JwtPayload.interface';

export interface ReqUser extends Request {
  user: JwtPayload;
}
