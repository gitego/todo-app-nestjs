import { DocumentBuilder, SwaggerCustomOptions } from '@nestjs/swagger';

export const config = new DocumentBuilder()
  .addBearerAuth({ type: 'http', scheme: 'bearer' })
  .setTitle('Todo App')
  .setDescription('The Todo App documentation')
  .setVersion('1.0.0')
  .addTag('Auth', 'Authentication Endpoints')
  .addTag('Todos', 'Todos Endpoints')
  .addTag('App Welcome', 'App welcome endpoint')
  .build();

export const customOptions: SwaggerCustomOptions = {
  swaggerOptions: {
    persistAuthorization: true,
  },
  customSiteTitle: 'Todo App API',
};
