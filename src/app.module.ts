import { Logger, Module } from '@nestjs/common';
import { APP_GUARD, APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { ThrottlerModule, ThrottlerGuard } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';
import config from '../ormconfig';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ExceptionsFilter } from './shared/filters/exception.filter';
import { LoggerInterceptor } from './shared/interceptors/logger.interceptor';
import { ResponseInterceptor } from './shared/interceptors/response.interceptor';
import { TodoModule } from './todo/todo.module';
import { TodoistModule } from './todoist/todoist.module';

@Module({
  imports: [
    ThrottlerModule.forRoot({
      ttl: 5,
      limit: 3,
    }),
    TypeOrmModule.forRoot(config),
    TodoModule,
    AuthModule,
    TodoistModule,
  ],
  controllers: [AppController],
  providers: [
    Logger,
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: APP_FILTER,
      useClass: ExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggerInterceptor,
    },
    AppService,
  ],
})
export class AppModule {}
