import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import constants from '../constants';
import { AuthService } from './auth.service';
import { JwtPayload } from '../shared/interfaces/JwtPayload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: constants.JWT_SECRET,
    });
  }

  /** Validate payload
   * @param payload user payload
   * @returns Promise<Todoist>
   */
  async validate(payload: JwtPayload) {
    return await this.authService.validateTodoist(payload);
  }
}
