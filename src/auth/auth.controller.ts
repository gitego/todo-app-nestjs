import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiResponse, ApiTags } from '@nestjs/swagger';
import { User } from '../shared/decorators/user.decorator';
import { LoginDto, SignupDto } from '../todoist/todoist.dto';
import { Todoist } from '../todoist/todoist.entity';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  /** Route: Sign up a todoist
   * @param signupDto the todoist data
   * @returns Promise<Todoist>
   */
  @Post('/signup')
  @ApiResponse({ status: 201, description: 'Sign up successful' })
  @ApiResponse({ status: 400, description: 'Bad entries' })
  async signup(@Body() signupDto: SignupDto) {
    return await this.authService.registerTodoist(signupDto);
  }

  /** Route: Log in a todoist
   * @param loginDto the todoist credentials
   * @returns Promise<access_token>
   */
  @Post('/login')
  @ApiResponse({ status: 201, description: 'Log in successful' })
  @ApiResponse({ status: 401, description: 'Invalid credentials' })
  async login(@Body() loginDto: LoginDto) {
    return await this.authService.login(loginDto);
  }

  @Get('/profile/:id')
  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({ status: 200, description: 'Success' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  async profile(@User() user: Todoist) {
    return { data: await this.authService.getProfile(user.id) };
  }
}
