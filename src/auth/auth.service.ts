import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginDto, SignupDto } from '../todoist/todoist.dto';
import { TodoistService } from '../todoist/todoist.service';
import { JwtPayload } from '../shared/interfaces/JwtPayload.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly todoistService: TodoistService,
    private readonly jwtService: JwtService,
  ) {}

  /** Service: Sign up a todoist
   * @param signupDto the todoist data
   * @returns Todoist
   */
  async registerTodoist(signupDto: SignupDto) {
    return await this.todoistService.createTodoist(signupDto);
  }

  /** Service: Log in a todoist
   * @param loginDto the todoist credentials
   * @returns access_token
   */
  async login(loginDto: LoginDto) {
    const todoist = await this.todoistService.findByLogin(loginDto);
    const access_token = this.jwtService.sign({ ...todoist });
    return { data: { access_token } };
  }

  /** Service: Validate a todoist
   * @param payload user payload
   * @returns Promise<Todoist>
   */
  async validateTodoist(payload: JwtPayload) {
    const todoist = await this.todoistService.findOne({
      where: { id: payload.id },
    });
    if (!todoist) throw new UnauthorizedException('Invalid token');
    return todoist;
  }

  async getProfile(id: string) {
    const todoist = await this.todoistService.findOne({
      where: { id },
      relations: ['todos'],
    });
    if (!todoist) throw new UnauthorizedException('Invalid token');
    delete todoist.password;
    return todoist;
  }
}
