import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  /** Service: Welcome method
   *
   * @returns message: string
   */
  getHello(): { message: string } {
    return { message: 'Welcome to the todo app API!' };
  }
}
