import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('App Welcome')
@Controller('/welcome')
export class AppController {
  constructor(private readonly appService: AppService) {}

  /** Route: Welcome endpoint
   *
   * @returns  message: string
   */
  @Get()
  getHello(): { message: string } {
    return this.appService.getHello();
  }
}
