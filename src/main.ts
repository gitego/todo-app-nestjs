import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { utilities, WinstonModule } from 'nest-winston';
import { transports, format } from 'winston';
import { AppModule } from './app.module';
import constants from './constants';
import { config, customOptions } from './shared/config/docs.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      levels: {
        error: 0,
        warn: 1,
        verbose: 3,
        info: 4,
        debug: 5,
        silly: 6,
      },
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        format.printf(
          (entry) => `${entry.timestamp} ${entry.level}: ${entry.message}`,
        ),
        format.colorize({ all: true }),
      ),
      transports: [
        new transports.File({
          filename: `${__dirname}/../logs/errors.log`,
          level: 'error',
        }),
        new transports.File({
          filename: `${__dirname}/../logs/traffic.log`,
          level: 'verbose',
        }),
        new transports.Console({
          format: format.combine(
            format.timestamp(),
            utilities.format.nestLike(),
          ),
          level: 'info',
        }),
      ],
    }),
  });

  /**
   * Add global prefix '<host>/api/'
   */
  app.setGlobalPrefix('api');

  /**
   * Use global validation pipe
   */
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
    }),
  );

  /**
   * Add security headers
   */
  app.use(helmet());

  /**
   * Restrict unused methods from being used
   */
  app.enableCors({
    methods: 'GET,POST,PATCH,DELETE',
  });

  /**
   * Set up docs
   */
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document, customOptions);

  /**
   * Start app
   */
  await app.listen(constants.PORT || 5000, () =>
    console.log(`Server listenning on ${constants.PORT || 5000}...`),
  );
}
bootstrap();
