import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, Length, MaxLength } from 'class-validator';

export class SignupDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(50, {
    message: 'First name is required and must be at most 50 characters',
  })
  @ApiProperty({
    description: 'The first name of a todoist',
    default: 'Brian',
  })
  firstName: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(50, {
    message: 'Last name is required and must be at most 50 characters',
  })
  @ApiProperty({
    description: 'The last name of a todoist',
    default: 'Gitego',
  })
  lastName: string;

  @IsNotEmpty()
  @IsString()
  @Length(5, 20, {
    message: 'Username is required and must be between 5 and 20 characters',
  })
  @ApiProperty({
    description: 'The username of a todoist',
    default: 'gbrian',
  })
  username: string;

  @IsNotEmpty()
  @IsString()
  @Length(6, 128, {
    message: 'Password must be between 6 and 128 characters',
  })
  @ApiProperty({
    description: 'The password of a todoist',
    default: 'Password',
  })
  password: string;
}

export class LoginDto {
  @IsNotEmpty({
    message: 'username is required',
  })
  @ApiProperty({
    description: 'The username of a todoist',
    default: 'gbrian',
  })
  username: string;

  @IsNotEmpty({
    message: 'password is required',
  })
  @ApiProperty({
    description: 'The password of a todoist',
    default: 'Password',
  })
  password: string;
}
