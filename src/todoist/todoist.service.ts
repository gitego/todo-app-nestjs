import {
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { Repository } from 'typeorm';
import { LoginDto, SignupDto } from './todoist.dto';
import { Todoist } from './todoist.entity';

@Injectable()
export class TodoistService {
  constructor(
    @InjectRepository(Todoist)
    private readonly todoistRepo: Repository<Todoist>,
  ) {}

  /** Service: Create/Register a todoist
   *
   * @param signupDto Sign up data
   * @returns Promise<{data: Todoist}>
   */
  async createTodoist(signupDto: SignupDto) {
    const todoist = await this.findOne({
      where: { username: signupDto.username },
    });
    if (todoist)
      throw new ConflictException('Todoist already exists, Log in instead?');
    const hash = await bcrypt.hash(signupDto.password, 10);
    let newTodoist = new Todoist();
    newTodoist = { ...newTodoist, ...signupDto, password: hash };
    await this.todoistRepo.save(newTodoist);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    delete newTodoist.password;
    return { data: newTodoist };
  }

  /** Service: Find and validate todoist by login credentials
   *
   * @param LoginDto Log in credentials
   * @returns Promise<Todoist>
   */
  async findByLogin({ username, password }: LoginDto) {
    const todoist = await this.findOne({ where: { username } });
    if (!todoist) throw new UnauthorizedException('Todoist not found');
    const match = await bcrypt.compare(password, todoist.password);
    if (!match) throw new UnauthorizedException('Invalid credentials');
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    delete todoist.password;
    return todoist;
  }

  /** Service: Find a todoist
   *
   * @param options Find options
   * @returns Promise<Todoist>
   */
  async findOne(options: any) {
    return await this.todoistRepo.findOne(options);
  }
}
