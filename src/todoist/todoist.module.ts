import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todo } from '../todo/todo.entity';
import { Todoist } from './todoist.entity';
import { TodoistService } from './todoist.service';

@Module({
  imports: [TypeOrmModule.forFeature([Todoist, Todo])],
  providers: [TodoistService],
  exports: [TodoistService],
})
export class TodoistModule {}
