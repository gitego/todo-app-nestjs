import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import constants from './src/constants';
import { Todo } from './src/todo/todo.entity';
import { Todoist } from './src/todoist/todoist.entity';

const config: TypeOrmModuleOptions = {
  type: 'postgres',
  host: constants.POSTGRES_HOST || 'localhost',
  port: 5432,
  username: constants.POSTGRES_USER,
  password: constants.POSTGRES_PASSWORD,
  database:
    constants.NODE_ENV === 'test'
      ? constants.POSTGRES_TEST_DB
      : constants.POSTGRES_DB,
  entities: [Todo, Todoist],
  synchronize: constants.NODE_ENV !== 'production',
  // autoLoadEntities: true,
  migrations: ['dist/src/db/migrations/*.js'],
  cli: {
    migrationsDir: 'src/db/migrations',
  },
};
export default config;
