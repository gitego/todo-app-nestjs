import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Todoist } from '../src/todoist/todoist.entity';
import { AppModule } from '../src/app.module';

describe('Auth', () => {
  let app: INestApplication;
  let todoistRepo: Repository<Todoist>;
  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    todoistRepo = moduleRef.get('TodoistRepository');
    await todoistRepo.query('delete from todoists');
  });
  afterAll(async () => {
    await todoistRepo.query('delete from todoists');
    await app.close();
  });
  it(`/POST signup`, () => {
    return request(app.getHttpServer())
      .post('/auth/signup')
      .send({
        firstName: 'Brian',
        lastName: 'Gitego',
        username: 'gbriantest',
        password: 'Password',
      })
      .expect(201);
  });

  it(`/POST login`, () => {
    return request(app.getHttpServer())
      .post('/auth/login')
      .send({
        username: 'gbriantest',
        password: 'Password',
      })
      .expect(201);
  });
});
